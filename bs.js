var historyApiFallback = require('connect-history-api-fallback');
var browserSync = require("browser-sync").create();

browserSync.init({
    files: [
      "build/**/*.{html,htm,css,js}"
    ],
    server: {
        baseDir: "build",
        middleware: [ historyApiFallback() ]
    }
});