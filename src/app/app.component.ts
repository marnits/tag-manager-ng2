import { Component } from '@angular/core';

declare var __moduleName: string;

@Component({
  selector: 'tag-manager',
  moduleId: __moduleName,
  templateUrl: 'app.html'
})

export class AppComponent { 
}